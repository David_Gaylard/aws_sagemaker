import boto3
from sagemaker.amazon.amazon_estimator import get_image_uri

xgb_image = get_image_uri(boto3.Session().region_name, 'xgboost', '0.90-1')

s3_xgb_output_key_prefix = "xgb_training_output"
s3_xgb_output_location = 's3://{}/{}/{}/{}/{}'.format(bucket, projectName, 'models', s3_xgb_output_key_prefix, 'xgb_model')
print("Model output to: {}".format(s3_xgb_output_location))

hyperparameters = {
        "max_depth":"6",
        "eta":"0.2",
        "gamma":"4",
        "min_child_weight":"6",
        "subsample":"0.2",
        "objective":"reg:squarederror",
        "num_round":"20",
        "eval_metric":"mae"}

xgb_estimator = sagemaker.estimator.Estimator(image_name=xgb_image, 
                                          hyperparameters=hyperparameters,
                                          role=role,
                                          train_instance_count=1, 
                                          train_instance_type='ml.m5.2xlarge', 
                                          train_volume_size=5, # 5 GB 
                                          output_path=s3_xgb_output_location)

data_channels = {'train': xgb_train_data,
                 'validation': xgb_val_data}

xgb_estimator.fit(inputs=data_channels, logs=True)