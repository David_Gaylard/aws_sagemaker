2020-07-06 17:05:17 Starting - Starting the training job...
2020-07-06 17:05:21 Starting - Launching requested ML instances......
2020-07-06 17:06:30 Starting - Preparing the instances for training...
2020-07-06 17:07:16 Downloading - Downloading input data...
2020-07-06 17:07:46 Training - Training image download completed. Training in progress.
2020-07-06 17:07:46 Uploading - Uploading generated training model.INFO:sagemaker-containers:Imported framework sagemaker_xgboost_container.training
INFO:sagemaker-containers:Failed to parse hyperparameter eval_metric value mae to Json.
Returning the value itself
INFO:sagemaker-containers:Failed to parse hyperparameter objective value reg:squarederror to Json.
Returning the value itself
INFO:sagemaker-containers:No GPUs detected (normal if no gpus installed)
INFO:sagemaker_xgboost_container.training:Running XGBoost Sagemaker in algorithm mode
INFO:root:Determined delimiter of CSV input is ','
INFO:root:Determined delimiter of CSV input is ','
INFO:root:Determined delimiter of CSV input is ','
[17:07:42] 2613x8 matrix with 20904 entries loaded from /opt/ml/input/data/train?format=csv&label_column=0&delimiter=,
INFO:root:Determined delimiter of CSV input is ','
[17:07:42] 1121x8 matrix with 8968 entries loaded from /opt/ml/input/data/validation?format=csv&label_column=0&delimiter=,
INFO:root:Single node training.
INFO:root:Train matrix has 2613 rows
INFO:root:Validation matrix has 1121 rows
[0]#011train-mae:85458.4#011validation-mae:86470.8
[1]#011train-mae:76228#011validation-mae:79385.8
[2]#011train-mae:69856.1#011validation-mae:75356.7
[3]#011train-mae:66686.8#011validation-mae:73013.3
[4]#011train-mae:64914.1#011validation-mae:72061.5
[5]#011train-mae:63497.2#011validation-mae:71315.9
[6]#011train-mae:62702.7#011validation-mae:71212.8
[7]#011train-mae:62037.2#011validation-mae:71273.4
[8]#011train-mae:61182#011validation-mae:71188.7
[9]#011train-mae:60612.1#011validation-mae:70740.6
[10]#011train-mae:60191.8#011validation-mae:71070.5
[11]#011train-mae:59806.4#011validation-mae:71383.1
[12]#011train-mae:59460#011validation-mae:71375.8
[13]#011train-mae:58962.1#011validation-mae:71426.6
[14]#011train-mae:58421.1#011validation-mae:72085.3
[15]#011train-mae:58483.2#011validation-mae:72677.9
[16]#011train-mae:58250.5#011validation-mae:72652.2
[17]#011train-mae:57979.7#011validation-mae:72808.5
[18]#011train-mae:57629.5#011validation-mae:73008.8
[19]#011train-mae:57045.3#011validation-mae:72549.4

2020-07-06 17:07:54 Completed - Training job completed
Training seconds: 38
Billable seconds: 38