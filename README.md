# AWS Sagemaker #
![Image of Sagemaker]
(images/sagemakerLogo.jpeg)

### About this Repository ###
This repository is designed as an introduction and warehouse of DFP Sagemaker tutorials and code. End-to-end example projects can be found under example_projects and individual functional pieces of code can be found under the subsequent example_ sections. 

### Getting started! ###
Begin by reading through this README and then jump into an example end-to-end project.

### What is Sagemaker? ###
* Sagemaker is an AWS service designed for Data Scientists for end-to-end data science project development including:
	* Batch data transformation,
		* AWS Batch
	* Model training and evaluation,
		* Preprocessors
		* Automatic hypertuning
	* Model deployment
		* Multi-model endpoint?
* AWS Sagemaker supports Boto3, PythonSDK and AWSCLI
	* recommended to use PythonSDK (via Jupyter Notebook) for projects.

### Good Principles ###
AWS Sagemaker is not designed to do long simplistic data transformations (although these can be built in with custom SKLearn transformers) and therefore when starting a project try to adhere to the following:

* Can I do more data manipulation on the database?
	* If YES - do so
	* If NO - build custom transformers, or
		* Batch transform with a different model and re-use output'd data
* Keep all code within the same S3 directory.
* Always split data into training/validation, reccomended 70/30 split.

### Sagemaker Architecture ###
Sagemaker has a complex architecture as it interacts with many different AWS services and trains on cloud, because of this it utilizes Docker environments and other additional environmental configurations that are specifically named and expected as core Sagemaker arguments. 
Below details the steps taken within both of the example projects (Abalone - public data, Revenue Estimate - internal data).

* Some of the key takeaways:
	* Sagemaker requires a AWS Role that has access to the servies it requires, these are typically:
		* Athena,
		* S3,
		* Sagemaker,
	* Setup project variables beforehand to keep all files within one S3 directory,
	* Sagemaker expects specific functions and configuration variables to be met,
	* Users can read .csv files through pd.read_csv("S3_path") directly from AWS S3 locations.

![Sagemaker Architecture]
(images/sagemakerArchitecture.jpg)

### Extra Sagemaker Resources and Learning! ###
Pluralsight: https://app.pluralsight.com/library/courses/aws-sagemaker-machine-learning-models/table-of-contents

### Who do I talk to? ###
* David Gaylard for questions and to approve code to submit.