import boto3
import time

#Function for executing athena queries
def run_query(query, s3_output):
    client = boto3.client('athena')
    response = client.start_query_execution(
        QueryString=query,
        
        ResultConfiguration={
            'OutputLocation': s3_output,
            }
        )
    
    query_id = response['QueryExecutionId'] # Save id
    
    # Keep polling until query is done
    completed = False
    while not completed:
        response = client.get_query_execution(QueryExecutionId=query_id)
        status = response['QueryExecution']['Status']['State']

        if status not in (
            'SUCCEEDED',
            'FAILED',
            'CANCELLED'
        ):
            print(f'Query {query_id} still running')
            time.sleep(1)
        else:
            if status != 'SUCCEEDED':
                raise Exception(response)
            completed = True
    return response

myQuery = """
select fixed_assets,
                   tangible_assets,
                   current_assets,
                   debtors,
                   cash_at_bank,
                   creditors_due_within_one_year,
                   net_current_liabilities,
                   net_assets,
                   turnover from(
select * from(
  select * from datalake.dim_company 
  where siccode_sictext_1 like  '%411__%'
  and companystatus_id is not null) dc
  left outer join datalake.fact_company_account_data ad on dc.company_number = 
  ad.company_number)
 where turnover between 0 and 500000; 

"""

projectName = "revenue_estimate-sic-411"
dataLocation = 's3://kiwikoder/{}/data'.format(projectName)

response = run_query(myQuery, dataLocation)
filePath = response['QueryExecution']['ResultConfiguration']['OutputLocation']