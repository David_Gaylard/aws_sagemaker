# Define a SKLearn Transformer from the trained SKLearn Estimator
prefix = "processed_data"
processed_data_output = 's3://{}/{}/{}/{}'.format(bucket, projectName,'data', prefix)

transformer = sklearn_preprocessor.transformer(
    instance_count=1, 
    instance_type='ml.m4.xlarge',
    assemble_with = 'Line',
    accept = 'text/csv',
    output_path = processed_data_output)


print(processed_data_output)
print(train_input)

# Preprocess training input
transformer.transform(train_input, content_type='text/csv')
print('Waiting for transform job: ' + transformer.latest_transform_job.job_name)
transformer.wait()
preprocessed_train = transformer.output_path