from sagemaker.model import Model
from sagemaker.pipeline import PipelineModel
import boto3
from time import gmtime, strftime

timestamp_prefix = strftime("%Y-%m-%d-%H-%M-%S", gmtime())

scikit_learn_inference_model = sklearn_preprocessor.create_model(env={"SAGEMAKER_DEFAULT_INVOCATIONS_ACCEPT": "text/csv"})

linear_learner_model = xgb_estimator.create_model()

model_name = 'revenue-estimate-pipeline-' + timestamp_prefix
endpoint_name = projectName.replace("_", "-") + "-EP"
sm_model = PipelineModel(
    name=model_name, 
    role=role, 
    models=[
        scikit_learn_inference_model, 
        linear_learner_model])

sm_model.deploy(initial_instance_count=1, instance_type='ml.c4.xlarge', endpoint_name=endpoint_name)