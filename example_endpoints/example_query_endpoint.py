from sagemaker.predictor import json_serializer, csv_serializer, json_deserializer, RealTimePredictor
from sagemaker.content_types import CONTENT_TYPE_CSV, CONTENT_TYPE_JSON

feature_columns_names = ['fixed_assets','tangible_assets','current_assets','debtors','cash_at_bank','creditors_due_within_one_year','net_current_liabilities','net_assets']

payload = df[81:95][feature_columns_names].to_csv(index = False)

actual_turnover = 51581

predictor = RealTimePredictor(
    endpoint=endpoint_name,
    sagemaker_session=sagemaker_session,
    serializer=csv_serializer,
    content_type=CONTENT_TYPE_CSV,
    accept=CONTENT_TYPE_JSON)

print(predictor.predict(payload))